<?php
/**
 * @license     Commercial
 * @author      Andre Engele <dev@andre-engele.de>
 * @version     1.0.0
 */


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController {

    /**
     * @Route("/", name="index")
     */
    public function index(){
        return $this->render('home/index.html.twig');
    }
}